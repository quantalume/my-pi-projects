#!/usr/bin/python

import time
import smbus
from Adafruit_I2C import Adafruit_I2C

def BCDdec(BCD):
    return (BCD & 0xF) + ((BCD & 0xF0) >> 4) * 10

def decBCD(dec):
    return dec // 10 * 16 + dec % 10
    
bus = smbus.SMBus(0)

addx = 0x68

'''set the time
bus.write_byte_data(addx, 5, decBCD(10))
bus.write_byte_data(addx, 4, decBCD(18))
bus.write_byte_data(addx, 6, decBCD(12))
bus.write_byte_data(addx, 2, decBCD(16))
bus.write_byte_data(addx, 1, decBCD(51))
bus.write_byte_data(addx, 0, 0)
'''

while True:
    month = BCDdec(bus.read_byte_data(addx, 5))
    day = BCDdec(bus.read_byte_data(addx, 4))
    year = BCDdec(bus.read_byte_data(addx, 6))
    hours = BCDdec(bus.read_byte_data(addx, 2))
    minutes = BCDdec(bus.read_byte_data(addx, 1))
    seconds = BCDdec(bus.read_byte_data(addx, 0))
    print('{0:02d}/{1:02d}/{2:02d}  {3:02d}:{4:02d}:{5:02d}'.format(month, day, year, hours, minutes, seconds))
    time.sleep(1)
